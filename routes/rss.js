var express = require('express');
var router = express.Router();
var feed = require('feed-read');
var format = require('date-format');

/* GET rss feed. */
router.get('/', function(req, res, next) {
  var items = [];
  feed('https://nodejs.org/en/feed/blog.xml', function(err, articles) {
    for (var i = 0; i < articles.length; i++) {
      var item = {};
      if(articles[i].title.includes("Node v")) {
        console.log(articles[i]);
        item.title = articles[i].title;
        item.link = articles[i].link;
        item.date = format('dd/MM/yyyy hh:mm', articles[i].published);

        items.push(item);
      }
    }
    res.render('rss', { title: 'RSS Feed', rssItems: items });
  });
});

module.exports = router;
